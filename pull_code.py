import os 
from subprocess import PIPE, Popen
import sys 

def execute_command(cmd: str):
    try:
        process = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()
        if process.returncode == 0:
            print(f"command {cmd} succeeded")
            print(out.decode('utf-8'))
        else:
            print(f"Command: {cmd} likely crashed, shell retruned code {process.returncode}")
            sys.exit(f"Error: {err.decode('utf-8')} , shell retruned code {process.returncode}")
    except OSError as e:
        sys.exit(f"failed to run shell: {e}")

# Config git credential helper
execute_command(
    cmd='git config credential.helper "/bin/bash deployments/credential-helper.sh"'
)
# Reset state
execute_command(
    cmd=f"git reset --hard HEAD && git checkout {os.getenv('GIT_BRANCH')}"
)
# Pull code from KSCM
execute_command(
    cmd="git pull"
)

# Checkout to commit
execute_command(
    cmd=f"git checkout {os.getenv('GIT_COMMIT')}"
)
